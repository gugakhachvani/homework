<?php 

namespace HomeWork\Controller;

use HomeWork\Data\Data;

Class Controller {

    private $operations;

    public $weekInfo = [];

    public $rates = [];


    public function __construct(){
        $this->operations = new Data;
        $this->fileName = $_SERVER['argv'][1];
        $this->rates = $this->operations::getRates();        
    }

    public function calculate(){

        $data = $this->operations->csvToArr($this->fileName);
        
        foreach($data as $day){
            $commission = round($this->{$day[3]."Calculate"}($day), 2, PHP_ROUND_HALF_UP);
            print $commission ."\r\n";
        }
    }

    public function depositCalculate($data){
        $exchangedAmount = $this->operations->exchange($data[4], $data[5], $this->rates);
        return $this->operations->calculateDeposit($exchangedAmount);
    }

    public function withdrawCalculate($data){

        $week = date('oW', strtotime($data[0]));
        $amountEuro = $this->operations->exchange($data[4], $data[5], $this->rates);
        $weekManipulation = $this->operations->weekManipulation($amountEuro, $week.'-'.$data[1], $this->weekInfo);             
        $comissionEuro = $weekManipulation[0];
        $this->weekInfo = $weekManipulation[1];
        $comission = $this->operations->calculateWithdraw($this->operations->exchangeRe($comissionEuro, $data[5], $this->rates));

        return $comission;
    }

}