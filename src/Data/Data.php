<?php 

namespace HomeWork\Data;

Class Data {

    public function csvToArr($fileName){

        $file = fopen($fileName, 'r');
        $returnArr = array();

        while(($line = fgetcsv($file)) !==false){
            array_push($returnArr, $line);
        }

        fclose($file);

        return $returnArr;
    }

    public function calculateDeposit($deposit){
        return $deposit / 100 * 0.03;
    }

    public function calculateWithdraw($withdraw){
        return $withdraw / 100 * 0.3;
    }

    public static function getRates(){
        return json_decode(file_get_contents('https://developers.paysera.com/tasks/api/currency-exchange-rates'));
    }

    public function exchange($amount, $currency, $rates){

        if($currency != $rates->base){
            return $amount / $rates->rates->{$currency};
        }

        return $amount;
    }

    public function exchangeRe($amount, $currency, $rates){
        return $amount * $rates->rates->{$currency};
    }

    public function weekManipulation($amount, $week, $weekInfo){     
        if($amount > 1000){
            $weekInfo[$week]['amount'] = $amount;
            $weekInfo[$week]['occurance'] = 3;
            $money = $amount - 1000;
        }else if(!isset($weekInfo[$week])){
            $weekInfo[$week]['amount'] = $amount;
            $weekInfo[$week]['occurance'] = 1;
            $money = 0;      
        }else{
            if($weekInfo[$week]['occurance'] >= 3){
                $money = $amount;
            }elseif($weekInfo[$week]['amount'] > 1000){
                $money = $amount;
            }else if($weekInfo[$week]['amount'] + $amount > 1000){
                $weekInfo[$week]['amount'] = $weekInfo[$week]['amount'] + $amount;
                $weekInfo[$week]['occurance'] = $weekInfo[$week]['occurance'] + 1;
                $money = $weekInfo[$week]['amount'] + $amount - 1000;
            }else{
                $weekInfo[$week]['amount'] = $weekInfo[$week]['amount'] + $amount;
                $weekInfo[$week]['occurance'] = $weekInfo[$week]['occurance'] + 1;
                $money = $amount;
            }
        }
        
        return[$money, $weekInfo];
    }
    
}